from typing import List
from typing import Dict


def find_it(seq: List) -> int:
    count: Dict = {}
    for num in seq:
        if num in count:
            count[num] += 1
        else:
            count[num] = 1

    # Find the number that appears an odd number of times
    for num, c in count.items():
        if c % 2 != 0:
            return num

    return


def find_it_xor(seq: List) -> int:
    result = 0
    for num in seq:
        result ^= num
    return result

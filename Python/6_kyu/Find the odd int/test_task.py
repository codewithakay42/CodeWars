from task import find_it
from task import find_it_xor


def test_one_item_in_list():
    seq = [1, 2, 1, 3, 3, 1, 2]
    exp = 1
    res = find_it(seq)
    assert res == exp


def test_with_xor():
    seq = [1, 2, 1, 3, 3, 1, 2]
    exp = 1
    res = find_it_xor(seq)
    assert res == exp

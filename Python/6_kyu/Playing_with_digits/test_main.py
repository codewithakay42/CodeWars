from main import dig_pow
from main import disect


def test_dig_pow():
    res = dig_pow(1, 1)
    assert isinstance(res, int)


def test_disect():
    res = disect(86)
    assert res == [8, 6]

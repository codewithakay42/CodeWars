import pytest
from task import longest


def test_longest():
    a = "xyaabbbccccdefww"
    b = "xxxxyyyyabklmopq"
    exp = "abcdefklmopqwxy"
    res = longest(a, b)
    assert res == exp

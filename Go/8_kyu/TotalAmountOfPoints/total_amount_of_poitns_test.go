package totalpoints

func TestCalculatePoints(t *testing.T) {
  tests := []struct{
    testName string
    matches []string
    expected int
  }{
    {testName: "all wins", matches: []string{"3:1", "3:2", "2:1"}, expected: 9},
    {testName: "all losses", matches: []string{"2:3", "1:3", "4:5"}, expected: 0},
    {testName: "assorted", matches: []string{"3:1", "1:4", "5:32", "42:42"}, expected: 4}
  }
  for _, tt := range tests {
    t.Run(tt.testName, func(t *testing.T){
      result := Points(tt.matches)
      if result != tt.expected {
        t.Error("got %d, want %d", result, tt.expected)
      }
    })
  }
}
